/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo() {
		let fullName = prompt("What is your name?")
		let age = prompt("How old are you?")
		let location = prompt("Where do you live?")

		console.log("Hello, " + fullName)
		console.log("You are " + age + " years old")
		console.log("You live in " + location)

		alert("Thank you for your input!")
	}

	getUserInfo()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function showBands() {
		let faveBands = ["The Juans", "December Avenue", "Tanya Markova", "Eraserheads", "The Beatles"]

		let count = 1

		for(let i = 0; i < 5; i++) {
			console.log(count + " " + faveBands[i])
			count++
		}
	}

	showBands()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//third function here:
	function showMovies() {
		let faveMovies = ["Encanto", "High School Musical", "High School Musical 2", "High School Musical 3: Senior Year", "Princess Diaries"]

		let movieRatings = [91, 65, 83, 64, 26]

		let count = 1

		for(let i = 0; i < 5; i++) {
			console.log(count + " " + faveMovies[i])
			count++

			console.log("Rotten Tomatoes Rating: " + movieRatings[i] + "%")
		}
	}

	showMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends() // add function call

// console.log(friend1);
// console.log(friend2);